Author: Maksym Zieliński
Desc: Application gives ability of storing users and deleting them via graphical interface. Storage is through Array List of User objects
and data visualisation is by table.
To add user, input data to ALL fields and click 'Add User'.
To remove user, click user on table and click 'Delete User'.

Instructions: Build project on App.java, Or run .jar in /target.

Created on Arch Linux.