package dev.com.maven.gui.components;

import javax.swing.*;

/**
 * Created by oogway on 15.01.17.
 */
public class OogPanel extends JPanel {
    public OogPanel(String text, int xPosition, int yPosition, int width, int height) {
        super(null);
        this.setLocation(xPosition, yPosition);
        this.setSize(width, height);
        this.setVisible(true);
    }

    public void alerting() {
        JFrame alert = new JFrame("Error");
        alert.setLocationRelativeTo(null);
        alert.setVisible(true);
        alert.setLayout(null);
        alert.setSize(300, 200);

        JButton close = new JButton("Close");
        close.setSize(80, 40);
        close.setLocation(110, 100);
        close.addActionListener(e1 -> alert.dispose());
        alert.add(new OogLabel("Enter valid values!", 80, 40, 180, 40));
        alert.add(close);
        alert.repaint();
    }
}
