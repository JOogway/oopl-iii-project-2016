package dev.com.maven.gui.components;

import javax.swing.*;

/**
 * Created by oogway on 15.01.17.
 */
public class OogList extends JList {
    public OogList() {
        this.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
        this.setLayoutOrientation(JList.HORIZONTAL_WRAP);
        this.setVisibleRowCount(-1);
        this.setVisible(true);
    }
}
