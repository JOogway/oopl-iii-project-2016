package dev.com.maven.gui.components;

import javax.swing.*;

/**
 * Created by oogway on 15.01.17.
 */
public class OogLabel extends JLabel {
    public OogLabel(String text, int xPosition, int yPosition, int width, int height) {
        super(text);
        this.setLocation(xPosition, yPosition);
        this.setSize(width, height);
        this.setVisible(true);
    }
}
