package dev.com.maven.gui.components;

import javax.swing.*;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;


/**
 * Created by oogway on 15.01.17.
 */
@SuppressWarnings("Since15")
public class OogTextField extends JTextField {
    public String text;

    public OogTextField(final String text, int xPosition, int yPosition, int width, int height) {
        super(text);
        this.text = text;
        this.setLocation(xPosition, yPosition);
        this.setSize(width, height);
        this.setVisible(true);
        this.addFocusListener(new FocusListener() {

            public void focusGained(FocusEvent e) {
                setText(null);
            }

            public void focusLost(FocusEvent e) {
                if (getText().equals("")) {
                    setText(text);
                }
            }
        });
    }
}

