package dev.com.maven.gui.components;

import javax.swing.*;

/**
 * Created by oogway on 15.01.17.
 */
public class OogScrollPane extends JScrollPane {
    public OogScrollPane(String text, int xPosition, int yPosition, int width, int height) {
        this.setName(text);
        this.setLocation(xPosition, yPosition);
        this.setSize(width, height);
        this.setVisible(true);
    }


}
