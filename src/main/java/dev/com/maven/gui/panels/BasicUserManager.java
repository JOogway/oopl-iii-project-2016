package dev.com.maven.gui.panels;

import dev.com.maven.gui.components.OogButton;
import dev.com.maven.gui.components.OogPanel;
import dev.com.maven.gui.components.OogTextField;
import dev.com.maven.objects.PremiumUser;
import dev.com.maven.objects.User;

import javax.swing.*;
import javax.swing.border.TitledBorder;
import java.awt.*;

/**
 * Created by oogway on 15.01.17.
 */

public class BasicUserManager extends OogPanel {
    public OogTextField nameUserField;
    public OogTextField surnameUserField;
    public OogTextField emailUserField;
    public OogButton addBasicClientButton;
    public OogButton eraseClientButton;

    public BasicUserManager(String text, int xPosition, int yPosition, int width, int height) {
        super(text, xPosition, yPosition, width, height);
        this.setBorder(new TitledBorder(BorderFactory.createTitledBorder("Basic User Panel")));
        nameUserField = new OogTextField("enter name", 10, 20, 150, 30);
        nameUserField.setToolTipText("Enter new user name");
        surnameUserField = new OogTextField("enter surname", 10, 50, 150, 30);
        surnameUserField.setToolTipText("Enter new user surname");
        emailUserField = new OogTextField("enter email", 10, 80, 150, 30);
        emailUserField.setToolTipText("Enter new user email");

        addBasicClientButton = new OogButton("Add User", 400, 120, 120, 30);
        eraseClientButton = new OogButton("Delete User", 400, 30, 120, 30);


        this.add(nameUserField);
        this.add(surnameUserField);
        this.add(emailUserField);
        this.add(addBasicClientButton);
        this.add(eraseClientButton);
        repaint();
    }

    public String[] returnUser(String name, String surname, String email) {
        String credits = "0";
        String points = "0";
        return new String[]{name, surname, email, credits, points};
    }

    public String[] clickAddEntry() {
        if (!nameUserField.getText().equals("enter name") && !surnameUserField.getText().equals("enter surname") && !emailUserField.getText().equals("enter email")) {
            return returnUser(nameUserField.getText(), surnameUserField.getText(), emailUserField.getText());
        } else {
            alerting();
            return null;
        }
    }

    public User clickAddUser() {
        if (!nameUserField.getText().equals("enter name") && !surnameUserField.getText().equals("enter surname") && !emailUserField.getText().equals("enter email")) {
            return new User(nameUserField.getText(), surnameUserField.getText(), emailUserField.getText());
        } else {
            alerting();
            return null;
        }
    }

}
