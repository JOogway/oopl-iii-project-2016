package dev.com.maven.gui.panels;

import dev.com.maven.gui.components.OogPanel;

import javax.swing.*;
import javax.swing.border.TitledBorder;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by oogway on 15.01.17.
 */
public class AccountTypeSwitch extends OogPanel {
    public JRadioButton basicAccountType = new JRadioButton("Basic", true);
    public JRadioButton premiumAccountType = new JRadioButton("Premium");

    public AccountTypeSwitch(String text, int xPosition, int yPosition, int width, int height, boolean basic) {
        super(text, xPosition, yPosition, width, height);
        this.setBorder(new TitledBorder(BorderFactory.createTitledBorder("User type")));
        ButtonGroup buttonGroup = new ButtonGroup();


        basicAccountType.setSize(150, 40);
        basicAccountType.setLocation(10, 20);
        basicAccountType.setVisible(true);

        premiumAccountType.setSize(150, 40);
        premiumAccountType.setLocation(10, 50);
        premiumAccountType.setVisible(true);

        buttonGroup.add(basicAccountType);
        buttonGroup.add(premiumAccountType);
        add(basicAccountType);
        add(premiumAccountType);
        this.repaint();


    }

    /*public String checkUserType(){
        if (this.basicAccountType.isSelected()){
            return "basic";
        }
        else if (this.premiumAccountType.isSelected()){
            return "premium";
        }
        else
            return "error";
    }*/

}
