package dev.com.maven.gui.panels;

/**
 * Created by oogway on 15.01.17.
 */

import dev.com.maven.gui.components.OogButton;
import dev.com.maven.gui.components.OogPanel;
import dev.com.maven.gui.components.OogTextField;
import dev.com.maven.objects.PremiumUser;
import dev.com.maven.objects.User;

import javax.swing.*;
import javax.swing.border.TitledBorder;

/**
 * Created by joogway on 08.01.17.
 */

public class PremiumUserManager extends OogPanel {
    public OogTextField nameUserField;
    public OogTextField surnameUserField;
    public OogTextField emailUserField;
    public OogTextField creditsUserField;
    public OogTextField pointsUserField;
    public OogButton addPremiumClientButton;
    public OogButton eraseClientButton;

    public PremiumUserManager(String text, int xPosition, int yPosition, int width, int height) {

        super(text, xPosition, yPosition, width, height);
        this.setBorder(new TitledBorder(BorderFactory.createTitledBorder("Premium User Panel")));

        nameUserField = new OogTextField("enter name", 10, 20, 150, 30);
        nameUserField.setToolTipText("Enter new user name");

        surnameUserField = new OogTextField("enter surname", 10, 50, 150, 30);
        surnameUserField.setToolTipText("Enter new user surname");

        emailUserField = new OogTextField("enter email", 10, 80, 150, 30);
        emailUserField.setToolTipText("Enter new user email");

        creditsUserField = new OogTextField("enter user credits", 170, 20, 150, 30);
        creditsUserField.setToolTipText("Enter user initial amount of credits");

        pointsUserField = new OogTextField("enter user points", 170, 50, 150, 30);
        pointsUserField.setToolTipText("Enter user initial amount of points");

        addPremiumClientButton = new OogButton("Add User", 400, 120, 120, 30);
        eraseClientButton = new OogButton("Delete User", 400, 30, 120, 30);
        eraseClientButton.setVisible(true);
        this.add(nameUserField);
        this.add(surnameUserField);
        this.add(emailUserField);
        this.add(creditsUserField);
        this.add(pointsUserField);
        this.add(addPremiumClientButton);
        this.add(eraseClientButton);
        repaint();
    }

    public String[] returnUser(String name, String surname, String email, String credits, String points) {
        return new String[]{name, surname, email, credits, points};
    }

    public String[] clickAddEntry() {
        if (!nameUserField.getText().equals("enter name") && !surnameUserField.getText().equals("enter surname") && !emailUserField.getText().equals("enter email") && !creditsUserField.getText().equals("enter user credits") && !pointsUserField.getText().equals("enter user points")) {
            return returnUser(nameUserField.getText(), surnameUserField.getText(), emailUserField.getText(), creditsUserField.getText(), pointsUserField.getText());
        } else {
            alerting();
            return null;
        }
    }

    public User clickAddUser() {
        if (!nameUserField.getText().equals("enter name") && !surnameUserField.getText().equals("enter surname") && !emailUserField.getText().equals("enter email") && !creditsUserField.getText().equals("enter user credits") && !pointsUserField.getText().equals("enter user points")) {
            return new PremiumUser(nameUserField.getText(), surnameUserField.getText(), emailUserField.getText(), creditsUserField.getText(), pointsUserField.getText());
        } else {
            alerting();
            return null;
        }
    }
}

