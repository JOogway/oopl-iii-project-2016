package dev.com.maven.objects;

/**
 * Created by joogway on 08.01.17.
 */
public class PremiumUser extends User implements dev.com.maven.templates.PremiumUser {

    int credits = 0;
    int points = 0;
    boolean premium = true;

    public PremiumUser(String name, String surname, String email, String credits, String points) {
        super(name, surname, email);
        addCredits(Integer.parseInt(credits));
        addPoints(Integer.parseInt(points));
        this.setPremium(premium);
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public void setSurname(String surname) {
        this.surname = surname;
    }

    @Override
    public void setEmail(String email) {
        this.email = email;
    }


    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public String getSurname() {
        return this.surname;
    }

    @Override
    public String getEmail() {
        return this.email;
    }


    @Override
    public void addCredits(int amount) {
        this.credits += amount;
    }

    @Override
    public void takeCredits(int amount) {
        this.credits -= amount;
    }

    @Override
    public void addPoints(int amount) {
        this.points += amount;
    }

    @Override
    public int getPoints() {
        return this.points;
    }

    @Override
    public int getRemainingCredits() {
        return this.credits;
    }
}
