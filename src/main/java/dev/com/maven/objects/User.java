package dev.com.maven.objects;

/**
 * Created by joogway on 08.01.17.
 */
public class User implements dev.com.maven.templates.User {
    String name, surname, email;
    int credits = 0;
    int points = 0;
    boolean premium = false;

    public User(String name, String surname, String email) {
        setName(name);
        setSurname(surname);
        setEmail(email);
        setPremium(false);

    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public void setSurname(String surname) {
        this.surname = surname;
    }

    @Override
    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public void setPremium(boolean isPremium) {
        this.premium = isPremium;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public String getSurname() {
        return this.surname;
    }

    @Override
    public String getEmail() {
        return this.email;
    }

    @Override
    public boolean isPremium() {
        return premium;
    }


}
