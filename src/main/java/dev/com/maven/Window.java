package dev.com.maven;

import dev.com.maven.gui.components.OogPanel;
import dev.com.maven.gui.panels.AccountTypeSwitch;
import dev.com.maven.gui.panels.BasicUserManager;
import dev.com.maven.gui.panels.PremiumUserManager;
import dev.com.maven.objects.User;

import javax.swing.*;
import javax.swing.plaf.ColorUIResource;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.util.ArrayList;

/**
 * Created by JOogway on 15.01.17.
 */

class Window extends JFrame {
    Window(String title, int width, int height) {
        this.setTitle(title);
        this.setSize(width, height);
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        this.setLayout(null);
        this.setLocationRelativeTo(null);
        this.setVisible(true);

        String[] columnNames = {"Name",
                "Surname",
                "Email",
                "Credits",
                "Points"};
        DefaultTableModel tableModel = new DefaultTableModel(columnNames, 0);
        JTable table = new JTable(tableModel);
        JScrollPane scrollPane = new JScrollPane(table);
        table.setFillsViewportHeight(true);
        ArrayList<User> usersCollection = new ArrayList<>();

        OogPanel mainPanel = new OogPanel("Users", 1, 1, 700, 300);
        mainPanel.setLayout(new BorderLayout());
        mainPanel.add(table.getTableHeader(), BorderLayout.PAGE_START);
        mainPanel.add(table, BorderLayout.CENTER);
        mainPanel.repaint();


        BasicUserManager buPanel = new BasicUserManager("", 10, 10, 550, 170);
        buPanel.addBasicClientButton.addActionListener(e -> {
            tableModel.addRow(buPanel.clickAddEntry());
            usersCollection.add(buPanel.clickAddUser());
            System.out.println();

            scrollPane.repaint();
            mainPanel.repaint();
        });

        buPanel.eraseClientButton.addActionListener(e -> {
            if (!usersCollection.isEmpty()) {
                usersCollection.remove(table.getSelectedRow());
                tableModel.removeRow(table.getSelectedRow());
            }
        });

        PremiumUserManager puPanel = new PremiumUserManager("", 10, 10, 550, 170);
        puPanel.addPremiumClientButton.addActionListener(e -> {
            tableModel.addRow(puPanel.clickAddEntry());
            usersCollection.add(puPanel.clickAddUser());
            scrollPane.repaint();
            mainPanel.repaint();
        });

        puPanel.eraseClientButton.addActionListener(e -> {
            if (!usersCollection.isEmpty()) {
                usersCollection.remove(table.getSelectedRow());
                tableModel.removeRow(table.getSelectedRow());
            }

        });
        OogPanel toolPanel = new OogPanel("Manager", 10, 360, 780, 200);
        toolPanel.setBorder(BorderFactory.createLineBorder(new ColorUIResource(106, 106, 106)));
        AccountTypeSwitch userTypeSwitch = new AccountTypeSwitch("switch", 570, 10, 190, 100, true);

        userTypeSwitch.basicAccountType.addActionListener(e -> {
            if (puPanel.getParent() == toolPanel)
                toolPanel.remove(puPanel);
            toolPanel.add(buPanel);
            toolPanel.repaint();
        });

        userTypeSwitch.premiumAccountType.addActionListener(e -> {
            if (buPanel.getParent() == toolPanel)
                toolPanel.remove(buPanel);
            toolPanel.add(puPanel);
            toolPanel.repaint();
        });

        toolPanel.add(buPanel);
        toolPanel.add(userTypeSwitch);


        addComponents(mainPanel, toolPanel);

        repaint();
    }

    private void addComponents(Component c1, Component c2) {
        this.add(c1);
        this.add(c2);
    }
}
