package dev.com.maven.templates;

/**
 * Created by joogway on 08.01.17.
 */
public interface User {
    void setName(String name);

    void setSurname(String surname);

    void setEmail(String email);

    void setPremium(boolean isPremium);

    String getName();

    String getSurname();

    String getEmail();

    boolean isPremium();
}
