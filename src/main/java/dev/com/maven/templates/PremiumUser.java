package dev.com.maven.templates;

/**
 * Created by joogway on 08.01.17.
 */

public interface PremiumUser extends User {
    void addCredits(int amount);

    void takeCredits(int amount);

    void addPoints(int amount);

    int getPoints();

    int getRemainingCredits();
}
